<?php
class magentoviavarejo_product extends viaVarejo_product
{
  public function __construct($api_client,$rest_client)
  {
    parent::__construct($api_client,$rest_client);
    $this->product_sku = '';
    $this->product_price = '';
    $this->product_stock = '';

    $this->wooCommerceProduct = new wooCommerceProduct;
  }

  public function magentoViaVarejoUpdateProduct()
  {
    if($this->product_price == '') echo "Preço do produto não pode ser vazio. valor: $this->product_price<br><h6>*Caso não haja valor mostrado em valor, o mesmo é nulo</h6>";
    else {
      $this->product_price = $this->product_price / (1-0.16);
      $update_price = $this->viaVarejo_updateProductPrice($this->product_sku,$this->product_price);
    }

    if($this->product_stock == '' && $this->product_stock != 0) echo "Estoque do produto não pode ser vazio. valor: $this->product_stock<br><h6>*Caso não haja valor mostrado em valor, o mesmo é nulo</h6>";
    else {
      if($this->product_stock < 0) $this->product_stock = 0;
      $update_stock = $this->viaVarejo_updateProductStock($this->product_sku,$this->product_stock);
    }

    if(is_null($update_price)) echo "Preço: <b>OK</b><br>";
    else {
      $error = new error_handling("Erro ao atualizar Preço do produto","Erro ao tentar atualizar Preço do produto", "Produto: $this->product_sku<br>Valor Preço: $this->product_price", "Erro produto");
      $error->send_error_email();
      $error->execute();
      echo "Erro ao atualizar o preço do produto $this->product_sku<br>";
    }

    if(is_null($update_stock)) echo "Stock: <b>OK</b><br>";
    else {
      $error = new error_handling("Erro ao atualizar Stock do produto","Erro ao tentar atualizar Stock do produto", "Produto: $this->product_sku<br>Valor Stock: $this->product_stock", "Erro produto");
      $error->send_error_email();
      $error->execute();
      echo "Erro ao atualizar o estoque do produto $this->product_sku<br>";
    }
  }

  public function createProduct($param)
  {
    if(empty($param['weight'])) $param['weight'] = '1';
    if(empty($param['length'])) $param['length'] = '1';
    if(empty($param['width'])) $param['width'] = '1';
    if(empty($param['height'])) $param['height'] = '1';


    $return = $this->viaVarejo_CreateProduct($param);
    // var_dump($return);
    // $httpCode = explode(" ",$return->response_status_lines[1]);

    if(!file_exists("productError.json")) file_put_contents("productError.json",json_decode(''));
    $skus = (array)json_decode(file_get_contents("productError.json"));

    // if((int)$httpCode[1] > 299) {
    if(!$return) {
      $skus[$param['sku']] = 'Nao criado';
      file_put_contents("productError.json",json_encode($skus));

      return false;
    }

    $skus[$param['sku']] = 'Criado';
    file_put_contents("productError.json",json_encode($skus));
    return "Criado com Sucesso";
  }
}
?>
