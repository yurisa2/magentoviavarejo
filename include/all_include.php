<?php
$prefix_m = 'include/apimagentophp/';
$prefix_viavarejo = 'include/apiviavarejophp/';
$prefix_wc = 'include/apiwoocommercephp/';

require_once 'include/apimagentophp/include/all_include.php';
require_once 'include/apiviavarejophp/include/all_include.php';
require_once 'include/sa2_flux/include/flux.php';
require_once 'include/apiwoocommercephp/include/all_include.php';

require_once 'include/magentoviavarejo_product.php';
require_once 'include/config.php';
require_once 'include/defines.php';

require_once 'include/event_base.php';
require_once 'include/error_handling.php';
require_once 'include/log.php';

require_once 'include/PHPMailer/src/Exception.php';
require_once 'include/PHPMailer/src/PHPMailer.php';
require_once 'include/PHPMailer/src/SMTP.php';

 ?>
